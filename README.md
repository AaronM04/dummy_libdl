Dummy libdl
===========

This is a shared library with nothing in it to avoid this linker error from `cargo build` or `cargo run`:

```
ld: error: unable to find library -ldl
```

The story here is that libdl.so on Linux has symbols that are in other places on BSD systems. Unlike FreeBSD (from what I hear), OpenBSD has no stub libdl.so to prevent linker errors, so we create one here.


# Usage

```
make install
```
