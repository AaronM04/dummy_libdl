OBJECTS=lib.o

all: libdl.so.1.0.1

lib.o: lib.c

libdl.so.1.0.1: $(OBJECTS)
	$(CC) -shared -Wl,-soname,libdl.so.1 -o libdl.so.1.0.1 $(OBJECTS) -lc

# FIXME: permissions
install: libdl.so.1.0.1
	doas cp /usr/local/lib/libdl.so.1.0.1  /usr/local/lib/rustlib/x86_64-unknown-openbsd/lib/libdl.so
